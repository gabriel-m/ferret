package templates

import (
	"gitlab.com/gabriel-m/ferret/pkg/drivers"
)

func WaitEventToEqOperator(when drivers.WaitEvent) string {
	if when == drivers.WaitEventPresence {
		return "=="
	}

	return "!="
}
