package templates

import (
	"fmt"

	"gitlab.com/gabriel-m/ferret/pkg/drivers/cdp/eval"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/values"
)

func StyleRead(name values.String) string {
	n := name.String()
	return fmt.Sprintf(
		`el.style[%s] != "" ? el.style[%s] : null`,
		eval.ParamString(n),
		eval.ParamString(n),
	)
}
