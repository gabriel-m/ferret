package templates

import (
	"fmt"

	"gitlab.com/gabriel-m/ferret/pkg/drivers"
	"gitlab.com/gabriel-m/ferret/pkg/drivers/cdp/eval"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/core"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/values"
)

func WaitBySelector(selector values.String, when drivers.WaitEvent, value core.Value, check string) string {
	return fmt.Sprintf(
		`
			const el = document.querySelector(%s); // selector

			if (el == null) {
				return false;
			}

			const result = %s; // check

			// when value
			if (result %s %s) {
				return true;
			}

			// null means we need to repeat
			return null;
`,
		eval.ParamString(selector.String()),
		check,
		WaitEventToEqOperator(when),
		eval.Param(value),
	)
}
