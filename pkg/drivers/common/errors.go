package common

import "gitlab.com/gabriel-m/ferret/pkg/runtime/core"

var (
	ErrReadOnly    = core.Error(core.ErrInvalidOperation, "read only")
	ErrInvalidPath = core.Error(core.ErrInvalidOperation, "invalid path")
)
