package literals

import (
	"context"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/core"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/values"
)

type StringLiteral string

func NewStringLiteral(str string) StringLiteral {
	return StringLiteral(str)
}

func (l StringLiteral) Exec(_ context.Context, _ *core.Scope) (core.Value, error) {
	return values.NewString(string(l)), nil
}
