package literals

import (
	"context"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/core"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/values"
)

type FloatLiteral float64

func NewFloatLiteral(value float64) FloatLiteral {
	return FloatLiteral(value)
}

func (l FloatLiteral) Exec(_ context.Context, _ *core.Scope) (core.Value, error) {
	return values.NewFloat(float64(l)), nil
}
