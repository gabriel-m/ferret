package literals

import (
	"context"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/core"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/values"
)

type noneLiteral struct{}

var None = &noneLiteral{}

func (l noneLiteral) Exec(_ context.Context, _ *core.Scope) (core.Value, error) {
	return values.None, nil
}
