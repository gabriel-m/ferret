package types

import (
	"context"

	"gitlab.com/gabriel-m/ferret/pkg/runtime/core"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/values"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/values/types"
)

// IsBool checks whether value is a boolean value.
// @param value (Value) - Input value of arbitrary type.
// @returns (Boolean) - Returns true if value is boolean, otherwise false.
func IsBool(_ context.Context, args ...core.Value) (core.Value, error) {
	err := core.ValidateArgs(args, 1, 1)

	if err != nil {
		return values.None, err
	}

	return isTypeof(args[0], types.Boolean), nil
}
