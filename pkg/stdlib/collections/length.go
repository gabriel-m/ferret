package collections

import (
	"context"

	"gitlab.com/gabriel-m/ferret/pkg/runtime/collections"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/core"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/values"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/values/types"
)

func Length(_ context.Context, inputs ...core.Value) (core.Value, error) {
	err := core.ValidateArgs(inputs, 1, 1)

	if err != nil {
		return values.None, err
	}

	value := inputs[0]

	c, ok := value.(collections.Measurable)

	if !ok {
		return values.None, core.TypeError(value.Type(),
			types.String,
			types.Array,
			types.Object,
			types.Binary,
			core.NewType("Measurable"),
		)
	}

	return c.Length(), nil
}
