package datetime

import (
	"context"

	"gitlab.com/gabriel-m/ferret/pkg/runtime/core"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/values"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/values/types"
)

// DateMinute returns the minute of date as a number.
// @params date (DateTime) - source DateTime.
// @return (Int) - a minute number.
func DateMinute(_ context.Context, args ...core.Value) (core.Value, error) {
	err := core.ValidateArgs(args, 1, 1)
	if err != nil {
		return values.None, err
	}

	err = core.ValidateType(args[0], types.DateTime)
	if err != nil {
		return values.None, err
	}

	min := args[0].(values.DateTime).Minute()

	return values.NewInt(min), nil
}
