package datetime_test

import (
	"testing"

	"gitlab.com/gabriel-m/ferret/pkg/stdlib/datetime"

	"gitlab.com/gabriel-m/ferret/pkg/runtime/core"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/values"
)

func TestNow(t *testing.T) {
	tcs := []*testCase{
		&testCase{
			Name:     "When too many arguments",
			Expected: values.None,
			Args: []core.Value{
				values.NewCurrentDateTime(),
			},
			ShouldErr: true,
		},
	}

	for _, tc := range tcs {
		tc.Do(t, datetime.Now)
	}
}
