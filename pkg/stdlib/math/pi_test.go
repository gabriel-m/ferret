package math_test

import (
	"context"
	"gitlab.com/gabriel-m/ferret/pkg/stdlib/math"
	. "github.com/smartystreets/goconvey/convey"
	m "math"
	"testing"
)

func TestPi(t *testing.T) {
	Convey("Should return Pi value", t, func() {
		out, err := math.Pi(context.Background())

		So(err, ShouldBeNil)
		So(out, ShouldEqual, m.Pi)
	})
}
