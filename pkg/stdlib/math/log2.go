package math

import (
	"context"
	"math"

	"gitlab.com/gabriel-m/ferret/pkg/runtime/core"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/values"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/values/types"
)

// Log2 returns the binary logarithm of a given value.
// @param number (Int|Float) - Input number.
// @returns (Float) - The binary logarithm of a given value.
func Log2(_ context.Context, args ...core.Value) (core.Value, error) {
	err := core.ValidateArgs(args, 1, 1)

	if err != nil {
		return values.None, err
	}

	err = core.ValidateType(args[0], types.Int, types.Float)

	if err != nil {
		return values.None, err
	}

	return values.NewFloat(math.Log2(toFloat(args[0]))), nil
}
