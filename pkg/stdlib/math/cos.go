package math

import (
	"context"
	"math"

	"gitlab.com/gabriel-m/ferret/pkg/runtime/core"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/values"
	"gitlab.com/gabriel-m/ferret/pkg/runtime/values/types"
)

// Cos returns the cosine of a given number.
// @param number (Int|Float) - Input number.
// @returns (Float) - The cosine of a given number.
func Cos(_ context.Context, args ...core.Value) (core.Value, error) {
	err := core.ValidateArgs(args, 1, 1)

	if err != nil {
		return values.None, err
	}

	err = core.ValidateType(args[0], types.Int, types.Float)

	if err != nil {
		return values.None, err
	}

	return values.NewFloat(math.Cos(toFloat(args[0]))), nil
}
