package stdlib

import (
	"gitlab.com/gabriel-m/ferret/pkg/runtime/core"
	"gitlab.com/gabriel-m/ferret/pkg/stdlib/arrays"
	"gitlab.com/gabriel-m/ferret/pkg/stdlib/collections"
	"gitlab.com/gabriel-m/ferret/pkg/stdlib/html"
	"gitlab.com/gabriel-m/ferret/pkg/stdlib/math"
	"gitlab.com/gabriel-m/ferret/pkg/stdlib/objects"
	"gitlab.com/gabriel-m/ferret/pkg/stdlib/strings"
	"gitlab.com/gabriel-m/ferret/pkg/stdlib/types"
	"gitlab.com/gabriel-m/ferret/pkg/stdlib/utils"
)

func RegisterLib(ns core.Namespace) error {
	if err := types.RegisterLib(ns); err != nil {
		return err
	}

	if err := strings.RegisterLib(ns); err != nil {
		return err
	}

	if err := math.RegisterLib(ns); err != nil {
		return err
	}

	if err := collections.RegisterLib(ns); err != nil {
		return err
	}

	if err := arrays.RegisterLib(ns); err != nil {
		return err
	}

	if err := objects.RegisterLib(ns); err != nil {
		return err
	}

	if err := html.RegisterLib(ns); err != nil {
		return err
	}

	return utils.RegisterLib(ns)
}
